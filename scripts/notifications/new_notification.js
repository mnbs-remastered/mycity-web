const convertStringIntoTimestamp = (s_date, s_hour) => {
    let year = Number(s_date.substring(0, 4));
    let month = Number(s_date.substring(5, 7));
    let day = Number(s_date.substring(8, 10));
    let hour = Number(s_hour.substring(0, 2));
    let minute = Number(s_hour.substring(3, 4));

    return new Date(year, month, day, hour, minute, 0, 0);
};

const formInputsForNewNotification = () => {
    let result = $('<form id="new_notification"></form>');
    let type = $('<select id="type" placeholder="Тип сповіщення">'
                    + '<option>Відключення води</option>'
                    + '<option>Відключення газу</option>'
                    + '<option>Відключення електроенергії</option>'
                    + '<option>Дорожні проблеми</option>'
                    + '<option>Транспортні проблеми</option>'
                    + '<option>Важливі події в місті</option>'
                + '</select>');
    let beginDate = $('<p>Від'
                        + ' <input id="from_date" type="date">'
                        + '<input id="from_hour" type="time">'
                     + '</p>');
    let endDate = $('<p id="end_date">До'
                        + '<input id="to_date" type="date"> '
                        + '<input id="to_hour" type="time"> '
                     + '</p>');
    let locality = $('<input id="locality" type="text" placeholder="Місто/село" class="long_input">');
    let region = $('<input id="region" type="text" placeholder="Район" class="long_input">');
    let street = $('<input id="street" type="text" placeholder="Вулиця" class="long_input">');
    let from_house = $('<input id="from_house" class="house_input" type="number" placeholder="Від">');
    let to_house = $('<input id="to_house" class="house_input" type="number" placeholder="До">');

    let submit = $('<button id="submit"></button>');

    submit.on("click", () => {
        notifications_db.doc().set({
            type: $('#type').val(),
            beginDate: convertStringIntoTimestamp($('#to_date').val(), $('#to_hour').val()),
            endDate: convertStringIntoTimestamp($('#from_date').val(), $('#from_hour').val()),
            town: $('#locality').val(),
            region: $('#region').val(),
            street: $('#street').val(),
            houses: $('#from_house').val() + '-' + $('#to_house').val(),
            information: "Нема інфи"
        });
        console.log("Done");
    });

    let dates_complex = $('<div id="dates_complex"></div>');
    let dates = $('<span id="dates"></span>');
    let dates_icon = $('<img src="../../mycity-web/images/dates.png" alt="" width="24" height="24" id="calendar">');

    let city_complex = $('<div id="city_complex"></div>');
    let city = $('<span id="city"></span>');
    let city_icon = $('<img class="icon" src="../../mycity-web//images/city.png" alt="" width="24" height="24">');

    let street_complex = $('<div id="street_complex"></div>');
    let streets = $('<span id="streets"></span>');
    let street_icon = $('<img class="icon" src="../../mycity-web/images/street.png" alt="" width="24" height="24">');
    let houses_range = $('<span id="houses_range"></span>');
    let description = $('<input id="description" type="text" placeholder="Опис причини">');

    dates.append(beginDate);
    dates.append(endDate);

    dates_complex.append(dates_icon);
    dates_complex.append(dates);

    city.append(locality);
    city.append(region);

    city_complex.append(city_icon);
    city_complex.append(city);

    houses_range.append(from_house);
    houses_range.append(to_house);

    streets.append(street);
    streets.append(houses_range);

    street_complex.append(street_icon);
    street_complex.append(streets);

    result.append(type);
    result.append(dates_complex);
    result.append(city_complex);
    result.append(street_complex);
    result.append(submit);
    result.append(description);

    return result;
};
