firebase.initializeApp({
    apiKey: "AIzaSyAlAAk6DWOwLm_nHTXy8reK4IxtkyXyu1E",
    authDomain: "mycity-mnbs.firebaseapp.com",
    databaseURL: "https://mycity-mnbs.firebaseio.com",
    projectId: "mycity-mnbs",
    storageBucket: "mycity-mnbs.appspot.com",
    messagingSenderId: "314208248396"
});

const notifications_db = firebase.firestore().collection("Cities").doc("Lviv").collection("Notifications");

const formNotification = (notification_data, id) => {
    let convertedNotification = $(`<li class='notification' id='${id}'></li>`);

    let type = $("<h3 align='center' id='title'></h3>").text(notification_data.type);

    let from_date = $("<div></div>").text(getDate(notification_data.beginDate));
    let to_date = $("<div></div>").text(getDate(notification_data.endDate));

    let region = $("<div></div>").text(notification_data.region);
    let locality = $("<div></div>").text(notification_data.town);
    let street = $("<div></div>").text(notification_data.street);
    let houses = $("<div></div>").text(notification_data.houses);

    let dates_complex = $('<span id="dates_complex_all"></span>');
    let dates_all = $('<span id="dates_all"></span>');
    let dates_icon = $('<img class="icon_all" src="../../mycity-web/images/dates.png" alt="" width="24" height="24">');

    let city_complex = $('<span id="city_complex_all"></span>');
    let city = $('<span id="city_all"></span>');
    let city_icon = $('<img class="icon_all" src="../../mycity-web//images/city.png" alt="" width="24" height="24">');

    let street_complex = $('<span id="street_complex_all"></span>');
    let streets = $('<span id="streets_all"></span>');
    let street_icon = $('<img class="icon_all" src="../../mycity-web/images/street.png" alt="" width="24" height="24">');
    let description = $('<p id="description">').text(notification_data.description);

    dates_all.append(from_date);
    dates_all.append(to_date);

    dates_complex.append(dates_icon);
    dates_complex.append(dates_all);

    city.append(locality);
    city.append(region);

    city_complex.append(city_icon);
    city_complex.append(city);

    streets.append(street);
    streets.append(houses);

    street_complex.append(street_icon);
    street_complex.append(streets);

    convertedNotification.append(type);
    convertedNotification.append(dates_complex);
    convertedNotification.append(city_complex);
    convertedNotification.append(street_complex);
    convertedNotification.append(description);

    return convertedNotification;
};
