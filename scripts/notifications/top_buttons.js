$('#complaints_button').on('click', () => {
    window.open("./index.html","_self","", true);
});


const clearPage = () => {
    $("#notifications").text("");
    $("#new_notification").remove();
};


$('#all_notifications').on('click', () => {
    $('#head_buttons').children().css({"color": "#757575", "border-color": "#757575"});
    $("#all_notifications").css({"color": "#00c597", "border-color": "#00c597"});
    clearPage();
    notifications_db.get().then(querySnapshot => {
        querySnapshot.forEach(doc => {
            $("#notifications").append(formNotification(doc.data(), doc.id));
        });
    });
});



$('#notification_form').on('click', () => {
    $('#head_buttons').children().css({"color": "#757575", "border-color": "#757575"});
    $("#notification_form").css({"color": "#00c597", "border-color": "#00c597"});
    clearPage();
    $('#notifications').before(formInputsForNewNotification());
});


$("#all_notifications").trigger("click");