const pad = n => {
    return (n < 10) ? ('0' + n) : n;
};

const getDate = (timestamp) => {

    let date = new Date(timestamp.seconds * 1000);

    let info = "";
    info += pad(date.getHours()) + ":";
    info += pad(date.getMinutes()) + "  ";
    info += pad(date.getDate()) + ".";
    info += pad(date.getMonth() + 1) + ".";
    info += pad(date.getFullYear());

    return info;
};

const setDeadline = () => new Date(Date.now() + 14 * 24 * 3600 * 1000);

const curDate = () => new Date(Date.now());

const range = (start, end) => {
    let result = [];
    for(let i = start; i <= end; i++) {
        result.push(i)
    }
    return result;
};