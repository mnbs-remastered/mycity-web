const formSentComplaint = (complaint_data, id) => {
    let convertedComplaint = $(`<li class='sent_complaint' id='${id}'></li>`);
    let head = $("<div class='complaint_head'></div>");
    let info = $("<div class='complaint_info'></div>");
    let companyTimeout = $("<div class='company_timeout'></div>");
    let submitComplex = $("<div class='submit_complex'></div>");

    let title = $("<p id='complaint_title_sent'></p>").text(complaint_data.title);
    let image = $(`<img class='complaint_image' src='${complaint_data.imageUrl}' alt=""/>`);
    let type = $("<div class='complaint_type'></div>").text(complaint_data.type);
    let date = $("<div class='complaint_date'></div>").text(getDate(complaint_data.date));
    let address = $("<div class='complaint_address'></div>").text(complaint_data.address);
    let description = $("<div class='complaint_description'></div>").text(complaint_data.description);
    let company = $("<input type='text' class='input_company' placeholder='Відповідальна установа'>");
    let timeout = $("<input class='timeout' type='text' placeholder='Час виконання'>");

    let submit = $("<button class='submit'></button>");
    let reject = $("<button class='reject'></button>");

    submit.on("click", () => {
        let inp = $("#" + `${id}`).find("input").val();
        if (inp !== '') {
            complaints_db.doc(id).update({"status": `Вирішується`,
                "company": `${inp}`,
                "deadline": setDeadline()});
            setTimeout($("#update_sent").trigger("click"), 3000);
        } else {
            alert('Вкажіть відповідальну установу');
        }
    });

    reject.on("click",  () => {
        const report = prompt("Чому ви відхилили цю скаргу?");
        if (report !== '') {
            complaints_db.doc(id).update({"status": `Відхилено`, "feedback": `${report}`});
            setTimeout($("#update_sent").trigger("click"), 3000);
        } else {
            alert('Поясніть причину відхилення.');
        }
    });

    info.append(title);
    info.append(type);
    info.append(date);
    info.append(address);

    head.append(image);
    head.append(info);
    head.append(reject);

    companyTimeout.append(company);
    companyTimeout.append(timeout);

    submitComplex.append(companyTimeout);
    submitComplex.append(submit);

    convertedComplaint.append(head);
    convertedComplaint.append(description);
    convertedComplaint.append(submitComplex);

    return convertedComplaint;
};
