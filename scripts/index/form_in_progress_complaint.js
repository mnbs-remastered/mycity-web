const formInProgressComplaint = (complaint_data, id) => {
    let convertedComplaint = $(`<li class='in_progress_complaint' id='${id}'></li>`);
    let head = $("<div class='complaint_head'></div>");
    let info = $("<div class='complaint_info'></div>");
    let footer = $("<div class='complaint_footer'></div>");

    let title = $("<div class='complaint_title'></div>").text(complaint_data.title);
    let image = $(`<img class='complaint_image' src='${complaint_data.imageUrl}' alt=""/>`);
    let type = $("<div class='complaint_type'></div>").text(complaint_data.type);
    let company = $("<div class='complaint_company'></div>").text(complaint_data.company);
    let date = $("<div class='complaint_date'></div>").text(getDate(complaint_data.date));
    let address = $("<div class='complaint_address'></div>").text(complaint_data.address);
    let description = $("<div class='in_progress_complaint_description'></div>").text(complaint_data.description);
    let deadline = $("<div class='complaint_deadline'></div>").text(getDate(complaint_data.deadline));
    let feedback =  $("<input class='input_feedback' placeholder='Коментар'>");
    let solveComplex = $("<div class ='solve_complex'></div>");
    let solve = $("<button class='solve'></button>");

    solve.on("click", () => {
        let inp = $("#" + `${id}`).find("input").val();
        if (inp !== '') {
            complaints_db.doc(id).update({
                "status": `Вирішено`,
                "feedback": `${inp}`,
                "closedDate": curDate()
            });
            setTimeout($("#update_in_progress").trigger("click"), 3000);
        } else {
            alert("Дайте короткий коментар про вирішення цієї скарги");
        }
    });

    info.append(title);
    info.append(type);
    info.append(date);
    info.append(address);

    head.append(image);
    head.append(info);

    footer.append(company);
    footer.append(deadline);
    footer.append(feedback);

    solveComplex.append(footer);
    solveComplex.append(solve);

    convertedComplaint.append(head);
    convertedComplaint.append(description);
    convertedComplaint.append(solveComplex);

    return convertedComplaint;
};
