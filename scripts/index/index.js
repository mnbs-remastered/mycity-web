firebase.initializeApp({
    apiKey: "AIzaSyAlAAk6DWOwLm_nHTXy8reK4IxtkyXyu1E",
    authDomain: "mycity-mnbs.firebaseapp.com",
    databaseURL: "https://mycity-mnbs.firebaseio.com",
    projectId: "mycity-mnbs",
    storageBucket: "mycity-mnbs.appspot.com",
    messagingSenderId: "314208248396"
});

const complaints_db = firebase.firestore().collection("Cities").doc("Lviv").collection("Complaints");

const formComplaint = (complaint, id) => {
    if (complaint.status === 'Надіслано') {
        return formSentComplaint(complaint, id);
    }
    else if (complaint.status === 'Вирішується') {
        return formInProgressComplaint(complaint, id);
    }
    else if (complaint.status === 'Вирішено') {
        return formSolvedComplaint(complaint, id);
    }
    else if (complaint.status === 'Відхилено') {
        return formRejectedComplaint(complaint, id);
    }
};

const addUpdateListener = (updateButton, currentStatus) => {
    updateButton.on("click",  () => {
        $("#head_buttons").children().css({"color": "#757575", "border-color": "#757575"});
        updateButton.css({"color": "#00c597", "border-color": "#00c597"});

        $("#complaints").text("");
        complaints_db.where("status", "==",`${currentStatus}`).orderBy("date","desc").get()
            .then(querySnapshot => {
                querySnapshot.forEach(doc => {
                    $("#complaints").append(formComplaint(doc.data(), doc.id));
                });
            })
            .catch(error => {
                console.log(error);
            });
    });
};

$(function () {
    addUpdateListener($("#update_sent"), "Надіслано");
    addUpdateListener($("#update_in_progress"), "Вирішується");
    addUpdateListener($("#update_solved"), "Вирішено");
    addUpdateListener($("#update_rejected"), "Відхилено");
});

$(function () {
    $("#notifications_button").on("click", () => {
        window.open("./notifications.html","_self","", true);
    })
});
