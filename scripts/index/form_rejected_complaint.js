const formRejectedComplaint = (complaint_data, id) => {
    let convertedComplaint = $(`<li class='rejected_complaint' id='${id}'></li>`);
    let head = $("<div class='complaint_head'></div>");
    let info = $("<div class='complaint_info'></div>");
    let footer = $("<div class='complaint_footer'></div>");

    let title = $("<div class='complaint_title'></div>").text(complaint_data.title);
    let image = $(`<img class='complaint_image' src='${complaint_data.imageUrl}' alt=""/>`);
    let type = $("<div class='complaint_type'></div>").text(complaint_data.type);
    let date = $("<div class='complaint_date'></div>").text(getDate(complaint_data.date));
    let address = $("<div class='complaint_address'></div>").text(complaint_data.address);
    let description = $("<div class='complaint_description'></div>").text(complaint_data.description);
    let feedback = $("<div class='complaint_report'></div>").text(complaint_data.feedback);

    info.append(title);
    info.append(type);
    info.append(date);
    info.append(address);

    head.append(image);
    head.append(info);

    footer.append(feedback);

    convertedComplaint.append(head);
    convertedComplaint.append(description);
    convertedComplaint.append(footer);

    return convertedComplaint;
};
